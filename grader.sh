#!/bin/bash



for student in $(ls | grep "[a-Z0-9\-]*.sh$" | cut -d'-' -f3 | cut -d'.' -f1 | sort -u)
do
    clear
    for file in $(ls *$student*)
    do
        title=$(echo $file | grep -o "[hl][wa][0-9][0-9].*")
        assignment=$(echo $title | cut -d"-" -f1)
        task=$(echo $title | cut -d"-" -f2)
        asurite=$(echo $title | cut -d"-" -f3 | cut -d"." -f1) 
        late=$(echo $file | grep -o "LATE")
        echo "#################################"
        echo "# $assignment $task $asurite $late"
        echo "#################################"
        echo "Script Contents:"
        echo ""
        cat *$assignment-$task-$asurite*
        echo ""

        echo "Script output:"
        echo ""
        echo "" | bash *$assignment-$task-$asurite* | head
        echo ""
    done
    read -p "You are viewing $student. Press enter to view the next student"
done
